# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2024-08-14

Update unity version and packages.

### Added

- EyapLibrary package added by default (can be removed in PackageManager).

### Changed

- Unity version is now Unity 6 Preview.
- Remove easings package.
- All packages were updated to latest available version.

## [1.0.0] - 2022-05-25

First version of the template.

### Added

- gitignore configured.
- git lfs set up with gitattributes.
- Editor configuration configured for C# writing conventions.
- The Unity project into a subfolder, with the starting packages for URP (including Shader Graph).
- New input system basic layout set-up.
- GameLibrary packages already in.
- Basic scene layout done.
