# Game Template

**A template to start a Unity project with the latest beta version and the URP rendering pipeline.**

It already has :
- gitignore configured.
- git lfs set up with gitattributes.
- Editor configuration configured for C# writing conventions.
- The Unity project into a subfolder, with the starting packages for URP (including Shader Graph).
- New input system basic layout set-up.
- GameLibrary packages already in.
- Basic scene layout done.
